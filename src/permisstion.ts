import router from '@/router'
import { getCookie } from '@/utils/cookie'
import store from './store'

router.beforeEach(async (to, from, next) => {
  const token = getCookie()
  const href = (to as any).href
  let usercode: string
  console.log(to.path)
  if (token) {
    let user = store.getters.user
    if (JSON.stringify(user) === '{}') {
      user = await store.dispatch('GetUserInfo')
      if (to.path === '/card/detail/') {
        usercode = href.split('=')[1]
        router.push({
          name: 'CardDetail',
          params: {
            usercode: usercode
          }
        })
      }
      if (to.path === '/card/detail/') {
        usercode = href.split('=')[1]
        router.push({
          name: 'CardDetail',
          params: {
            usercode: usercode
          }
        })
      }
      next()
    }

    next()
  } else {
    if (process.env.NODE_ENV === 'production') {
      // if (to.path === '/card/detail') {
      //   window.location.href = process.env.VUE_APP_BASE_URL + '/v/weixin/oauth2/web?card=1'
      // }
      window.location.href = process.env.VUE_APP_BASE_URL + '/v/weixin/oauth2/web'
    } else {
      let user = store.getters.user
      if (JSON.stringify(user) === '{}') {
        user = await store.dispatch('GetUserInfo')
        if (to.path === '/card/detail/') {
          usercode = href.split('=')[1]
          router.push({
            name: 'CardDetail',
            params: {
              usercode: usercode
            }
          })
        }
        next()
      }
      if (to.path === '/card/detail/') {
        usercode = href.split('=')[1]
        router.push({
          name: 'CardDetail',
          params: {
            usercode: usercode
          }
        })
      }
      next()
    }
  }
})

export default router
