import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/card',
    name: 'Home',
    component: Home
  },
  {
    path: '/newcard/:newType',
    name: 'NewCard',
    meta: {
      cache: true
    },
    component: () => import('@/views/commitCard/newCard.vue')
  },
  {
    path: '/card/all',
    name: 'CardAll',
    component: () => import('@/views/CardAll/index.vue')
  },
  {
    path: '/card/detail/?usercode=:usercode',
    name: 'CardDetail',
    component: () => import('@/views/CardDetail/index.vue')
  },
  {
    path: '/recard/:newType',
    name: 'ReCard',
    component: () => import('@/views/ReCard/index.vue')
  },
  {
    path: '/card/userdetail',
    name: 'UserCardDetail',
    component: () => import('@/views/UserCardDetail/index.vue')
  },
  {
    path: '/form/test',
    name: 'FormTest',
    component: () => import('@/views/test/index.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
