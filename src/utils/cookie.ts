import Cookies from 'js-cookie'
const CookieName = 'TOKEN'

export const getCookie = () => {
  return Cookies.get(CookieName)
}

export const setCookie = (value: string) => {
  return Cookies.set(CookieName, value)
}

export const removeCookie = () => {
  return Cookies.remove(CookieName)
}
