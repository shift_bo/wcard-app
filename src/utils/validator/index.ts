export function valNubStringHas (val: string|number): boolean {
  return val !== ''
}

export function valListLeng (val: Array<string>): boolean {
  return val.length !== 0
}

export function valPhone (val: string): boolean {
  return /^(13[0-9]|14[01456879]|15[0-3,5-9]|16[2567]|17[0-8]|18[0-9]|19[0-3,5-9])\d{8}$/.test(val)
}
