export function initAppHeight (): void{
  const clientHeight = document.documentElement.clientHeight
  const app: null | HTMLElement = document.getElementById('app')
  const appStyle = { height: clientHeight + 'px' }
  Object.assign(app?.style, appStyle)
}

export const _isMobile = () => {
  const flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)
  return flag
}
