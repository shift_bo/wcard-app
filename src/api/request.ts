import { AxiosPromise, AxiosResponse } from 'axios'
import request from '.'
import { FormData } from '@/views/commitCard/type'
import { IndexAnchor } from 'vant'

interface FlaskReponse<T> {
  [key: string]: T;
}

interface User {
  username: string;
  name: string;
  usercode: string;
  departmentId: number;
  roleId: number;
}

export interface CardData{
  username: string;
  usercode: string;
  name: string;
  commitTime: string;
  rejectMsg?: string;
  status: number;
}

// 获取用户数据
export const getUserInfo = (): AxiosPromise<FlaskReponse<User>> => {
  return request({
    url: '/v/currentuser'
  })
}

// 提交工卡申请
export const postCardDataY = (data: FormData) => {
  return request({
    url: '/api/cardata?newType=y',
    method: 'POST',
    data
  })
}
export const postCardDataN = (data: FormData) => {
  return request({
    url: '/api/cardata?newType=n',
    method: 'POST',
    data
  })
}
// 获取工卡列表
export const getCardData = (type: string, page: number): AxiosPromise<FlaskReponse<CardData>> => {
  return request({
    url: `/api/cardata?type=${type}&page=${page}`
  })
}

// 变更工卡状态
export const changeCardState = (data: {
  state: string;
  cardDataList: CardData[];
  rejectMsg?: string;
}): AxiosPromise<FlaskReponse<string>> => {
  return request({
    url: '/api/cardstate',
    method: 'post',
    data
  })
}

// 获取工卡详情
export const getDetialCardInfo = (usercode: string): AxiosPromise<FlaskReponse<CardData>> => {
  return request({
    url: `/api/cardDetial?usercode=${usercode}`
  })
}

export const sendMessageToUser = (data: {usercodeList: string[]}) => {
  return request({
    url: '/api/sendmessagetouser',
    method: 'post',
    data
  })
}

export const reCard = (data: any) => {
  return request({
    url: '/api/recard',
    method: 'post',
    data
  })
}

export const exportData = (data: any) => {
  return request({
    url: '/api/export',
    method: 'post',
    data
  })
}

// SDK
export const getSDKconfig = (url: string) => {
  return request({
    url: `/v/weixin/sdk?u=${url}`
  })
}
