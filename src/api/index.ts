import axios, { AxiosInstance, AxiosResponse } from 'axios'
import { Toast } from 'vant'
const request: AxiosInstance = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 6000
})

request.interceptors.response.use((response): AxiosResponse | Promise<AxiosResponse<string>> => {
  if (response.headers['content-type'] === 'zip' && response.status === 200) {
    return response
  }
  if (response.data.status === 200) {
    return response.data
  } else {
    const errMsg = response.data.errmsg
    console.log(errMsg)
    Toast({
      message: errMsg
    })
    return response.data
  }
})

export default request
