import { createStore, Store } from 'vuex'
import user from './module/user'
import getters from './getters'
import { RootState } from './types'

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties{
    $store: Store<RootState>;
  }
}

const store = {
  modules: {
    user
  },
  getters
}

export default createStore(store)
