import { RootState, UserState, User } from '../types'
import { ActionTree, MutationTree } from 'vuex'
import { getUserInfo } from '@/api/request'
const state: UserState = {
  info: {}
}

const mutations: MutationTree<UserState> = {
  SET_USER_INFO (state: UserState, userInfo) {
    state.info = userInfo
  }
}

const actions: ActionTree <UserState, RootState> = {
  async GetUserInfo ({ commit }): Promise<User> {
    const userInfo = (await getUserInfo()).data.currentUser
    commit('SET_USER_INFO', userInfo)
    return Promise.resolve(userInfo)
  }
}

export default {
  state,
  mutations,
  actions
}
