export interface UserState {
  info: User|{};
}

export interface User {
  username: string;
  name: string;
  usercode: string;
  departmentId: number;
  roleId: number;
}

export interface RootState {
  user: UserState;
}
