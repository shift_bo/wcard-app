import { RootState } from './types'

const getters = {
  user: (state: RootState) => state.user.info
}

export default getters
