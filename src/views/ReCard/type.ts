export interface FormData{
  username: string;
  usercode: string;
  name: string;
  userImage: {
    [index: number]: {
      content: string;
    };
  };
}
