export interface FormData{
  username: string;
  usercode: string;
  name: string;
  originType: any;
  userImage: {
    [index: number]: {
      content: string;
    };
  };
}
