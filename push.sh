#!/bin/sh
yarn build
zip -r card-app.zip dist/*
scp card-app.zip notice:/data/web_root
ssh notice < pull.sh